package css.mode;
#if macro
import css.RuleObject;
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;

class DefaultMode implements Mode
{
	public function new()
	{
	}

	function processField(target:Expr, targetId:String, field:ClassField, cssDeclaration:Dynamic):Null<Expr>
	{
		if(Std.is(cssDeclaration.value, String) || Std.is(cssDeclaration.value, Float) || Std.is(cssDeclaration.value, Int))
		{
			return haxe.macro.Context.parse(targetId + "." + field.name + ' = '+cssDeclaration.value, haxe.macro.Context.currentPos());
		}
		// if we got here then we need to write some custom logic for processing the cssDeclaration
		Context.warning("Unhandled cssDecalation:" + cssDeclaration, Context.currentPos());
		return null;
	}

	function mapField(target:Expr, targetId:String, fieldName:String, cssValue:Dynamic):Null<Expr>
	{
		return null;
	}

	public function processSelector(type:haxe.macro.Type, typeName:String, className:String, target:Expr, targetId:String, stateMap:StateMap):Null<Array<haxe.macro.Expr>>
	{
		var propertiesMap = TypeInfo.getPropertiesMapFor(type);
		var block:Array<haxe.macro.Expr> = [];

		// deal with default state first
		var defaultDeclarationMap:RuleMap = stateMap.get("*");
		if(defaultDeclarationMap == null)
		{
			trace("states: "+stateMap.keys());
			return null;
		}
		for (field in defaultDeclarationMap.keys())
		{
			if (!propertiesMap.exists(field))
			{
				Context.warning(typeName + " does not contain a field called '" + field + "', attempting to remap", Context.currentPos());
				var e = mapField(target, targetId, field, defaultDeclarationMap.get(field));
				if(e == null) continue;
				block.push(e);
			}
			else
			{
				var e = processField(target, targetId, propertiesMap.get(field), defaultDeclarationMap.get(field));
				block.push(e);
			}
		}

		if(block.length > 0) return block;
		return null;
	}

	public function isInline():Bool
	{
		return false;
	}
}
#end