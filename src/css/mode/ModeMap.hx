package css.mode;
#if !haxe3
class ModeMap extends Hash<css.mode.Mode>
{
    public function new()
	{
		super();
		set("default", new DefaultMode());
    }
}
#end
