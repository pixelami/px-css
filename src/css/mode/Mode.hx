package css.mode;

import css.RuleObject;
import haxe.macro.Type;
import haxe.macro.Expr;

interface Mode
{
	//function isInline():Bool;
	//function processField(target:Expr, targetId:String, field:ClassField, cssDeclaration:Dynamic):Null<Expr>;
	//function mapField(target:Expr, targetId:String, fieldName:String, cssDeclaration:Dynamic):Null<Expr>;
	function processSelector(type:haxe.macro.Type, typeName:String, cssClassName:String, target:Expr, targetId:String, declarationMap:StateMap):Null<Array<haxe.macro.Expr>>;
}
