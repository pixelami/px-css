package css;

import haxe.macro.Type;

class TypeInfo
{
	#if !haxe3
	static var typeFieldCache:Hash<Hash<ClassField>> = new Hash();
	#else
	static var typeFieldCache:Map<String, Map<String, ClassField>> = new Map();
	#end

	#if !haxe3
	public static function getPropertiesMapFor(type:Type, ?staticAccess:Bool=false):Hash<ClassField>
	#else
	public static function getPropertiesMapFor(type:Type, ?staticAccess:Bool=false):Map<String, ClassField>
	#end
	{
		return switch(type) {
			case Type.TInst(t, params):
				var cname = getFQCN(t.get()) + (staticAccess ? "#class" : "#instance");
				if (!typeFieldCache.exists(cname)) typeFieldCache.set(cname, getPropertiesMap(t.get(), staticAccess));
				typeFieldCache.get(cname);

			case Type.TType(t, params):
				getPropertiesMapFor(haxe.macro.Context.follow(t.get().type), staticAccess);

			case Type.TAnonymous(a): //typedef
				getAnonPropertiesMap(a.get());
				
			default:
				#if !haxe3
				new Hash<ClassField>();
				#else
				new Map<String, ClassField>();
				#end

		}
	}

	public static function getTypeNameFor(type:Type)
	{
		return switch(type) {
			case Type.TInst(t, params):
				t.get().name;
			case Type.TType(t, params):
				t.get().name;
			default:
				null;
		}
	}

	static function getFQCN(classType:ClassType):String
	{
		return classType.pack.join(".") + classType.name;
	}

	//TODO this might be optimized by also doing a cacheType lookup
	#if !haxe3
	static function getPropertiesMap(classType:ClassType, ?staticAccess:Bool=false):Hash<ClassField>
	#else
	static function getPropertiesMap(classType:ClassType, ?staticAccess:Bool=false):Map<String, ClassField>
	#end
	{
		#if !haxe3
		var properties:Hash<ClassField> = new Hash<ClassField>();
		#else
		var properties:Map<String, ClassField> = new Map<String, ClassField>();
		#end

		var fields:Array<ClassField> = staticAccess ? classType.statics.get() : classType.fields.get();

		var s = classType.superClass;
		while(s != null)
		{
			var _t:ClassType = s.t.get();
			fields = fields.concat( staticAccess ? _t.statics.get() : _t.fields.get() );
			s = _t.superClass;
		}
		
		for(f in fields) {
			switch(f.kind) {
				case FieldKind.FVar(read, write):
					// TODO check if property is writable
					properties.set(f.name, f);
				default:
			}
		}
		
		return properties;
	}
	
	#if !haxe3
	static function getAnonPropertiesMap(anon:AnonType):Hash<ClassField>
	#else
	static function getAnonPropertiesMap(anon:AnonType):Map<String, ClassField>
	#end
	{
		#if !haxe3
		var properties:Hash<ClassField> = new Hash<ClassField>();
		#else
		var properties:Map<String, ClassField> = new Map<String, ClassField>();
		#end

		var fields:Array<ClassField> = anon.fields;

		for(f in fields) {
			switch(f.kind) {
				case FieldKind.FVar(read, write):
					// TODO check if property is writable
					properties.set(f.name, f);
				default:
			}
		}
		
		return properties;
	}
}


