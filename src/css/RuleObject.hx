package css;

import pixelami.css.CSSParserToken;

typedef RuleDeclaration = {prefix:String, state:String, name:String, value:Dynamic, pos:Position}
#if !haxe3
typedef RuleMap = Hash<RuleDeclaration>;
typedef StateMap = Hash<RuleMap>;
typedef PrefixMap = Hash<StateMap>;
#else
typedef RuleMap = Map<String, RuleDeclaration>;
typedef StateMap = Map<String, RuleMap>;
typedef PrefixMap = Map<String, StateMap>;
#end

class RuleObject
{
	var prefixMap:PrefixMap;

	public var prefixes(get_prefixes, null):Iterator<String>;
	function get_prefixes():Iterator<String>
	{
		return prefixMap.keys();
	}

	public function new()
	{
		#if !haxe3
		prefixMap = new Hash();
		#else
		prefixMap = new Map();
		#end
	}

	public function add(prefix:String, state:String, name:String, value:Dynamic, pos:Position)
	{
		var stateMap:StateMap = getStateMapForPrefix(prefixMap, prefix);
		var ruleMap = getRuleMapForState(stateMap, state);
		ruleMap.set(name, {prefix:prefix, state:state, name:name, value:value, pos:pos});
	}

	public function getPrefix(prefix:String):StateMap
	{
		return getStateMapForPrefix(prefixMap, prefix);
	}

	public function getStateForPrefix(prefix, ?state = "*"):RuleMap
	{
		var stateMap = getStateMapForPrefix(prefixMap, prefix);
		return getRuleMapForState(stateMap, state);
	}

	function getStateMapForPrefix(prefixMap:PrefixMap, prefix:String):StateMap
	{
		if(prefixMap.exists(prefix)) return prefixMap.get(prefix);
		#if !haxe3
		var map:StateMap = new Hash();
		#else
		var map:StateMap = new Map();
		#end
		prefixMap.set(prefix, map);
		return map;
	}

	function getRuleMapForState(stateMap, state):RuleMap
	{
		if(stateMap.exists(state)) return stateMap.get(state);
		var map:RuleMap = new RuleMap();
		stateMap.set(state, map);
		return map;
	}

	public function iterator():Iterator<RuleDeclaration>
	{
	    return new RuleObjectIterator(this);
	}

	public function toString()
	{
		var buf:StringBuf = new StringBuf();
		for(declaration in iterator())
		{
			var d:RuleDeclaration = declaration;
			var posInfo = " (" + d.pos.filename + ":" + d.pos.lineNumber + ":" + d.pos.char + ")";
			buf.add("  "+d.prefix + " :" + d.state +" " + d.name + " = " +d.value + posInfo);

		}
		return buf.toString();
	}

}

class RuleObjectIterator
{
	var ruleObject:RuleObject;
	var stack:Array<Iterator<RuleDeclaration>>;
	var currentIterator:Iterator<RuleDeclaration>;

	public function new(ruleObject:RuleObject)
	{
		currentIterator = null;
		this.ruleObject = ruleObject;
		stack = [];

		for(prefix in ruleObject.prefixes)
		{
		    var map:StateMap = ruleObject.getPrefix(prefix);
			for(ruleMap in map.iterator())
			{
				stack.push(ruleMap.iterator());
			}
		}

		if(stack.length > 0) currentIterator = stack.shift();
	}

	public function hasNext():Bool
	{
	    if(currentIterator == null) return false;
		if(!currentIterator.hasNext() && stack.length > 0)
		{
			currentIterator = stack.pop();
			return true;
		}
		return currentIterator.hasNext();
	}

	public function next():RuleDeclaration
	{
		return currentIterator.next();
	}
}

class StateMapIterator
{
	var stateMap:StateMap;
	var stack:Array<Iterator<RuleDeclaration>>;
	var currentIterator:Iterator<RuleDeclaration>;

	public function new(stateMap:StateMap)
	{
		this.stateMap = stateMap;
		stack = [];
		for(ruleMap in stateMap.iterator())
		{
			stack.push(ruleMap.iterator());
		}
	}

	public function hasNext():Bool
	{
		if(!currentIterator.hasNext() && stack.length > 0)
		{
			currentIterator = stack.pop();
			return true;
		}
		return currentIterator.hasNext();
	}

	public function next():RuleDeclaration
	{
		return currentIterator.next();
	}
}