package css;

import css.mode.DefaultMode;
import css.RuleObject;
import haxe.macro.Expr;
import haxe.macro.Context;

class CSS
{
	#if macro

    // Dirty hack to dynamically include a class in a macro.
	static function importClass(fqcn:String)
	{
	    //Context.error("hey: "+trace(fcqn), Context.currentPos());
		#if !haxe3
		var uniqueName = "Importer_" + haxe.Md5.encode(fqcn);
		#else
		var uniqueName = "Importer_" + haxe.crypto.Md5.encode(fqcn);
		#end

		var sb:StringBuf = new StringBuf();
		var nl = "\n";
		sb.add("package;");
		sb.add(nl);

		#if !haxe3
		sb.add("@:macro class Importer__  {");
		sb.add(nl);
		sb.add("public static function build():haxe.macro.Type {");

		#else
		sb.add("class Importer__  {");
		sb.add(nl);
		sb.add("macro public static function build():haxe.macro.Type {");
		#end

		sb.add(nl);
		sb.add(fqcn + ";");
		sb.add(nl);
		sb.add('return haxe.macro.Context.getType("String");');
		sb.add(nl);
		sb.add("}");
		sb.add(nl);
		sb.add("}");
		sb.add(nl);
		sb.add("typedef " + uniqueName + " = haxe.macro.MacroType<[Importer__.build()]>;");
		sys.io.File.saveContent(uniqueName + ".hx", sb.toString());
		Context.getType(uniqueName);
		sys.FileSystem.deleteFile(uniqueName + ".hx");
	}


	static var resolver:CSSStyleRuleResolver;

	public static function styleSheet(cssFiles:Array<String>, ?vendor:String = "")
	{
		var stylesheets = [];
		for (i in 0...cssFiles.length)
		{
			stylesheets.push(StyleUtil.loadStyleSheet(cssFiles[i]));
		}

		resolver = new CSSStyleRuleResolver(stylesheets);
	}

	static function initModeMap():Map<String, css.mode.Mode>
	{
		var map:Map<String, css.mode.Mode> = new Map();
		map.set("default", new DefaultMode());
		return map;
	}

	#if !haxe3
	public static var prefixModeMap:css.mode.ModeMap = new css.mode.ModeMap();
	#else
	public static var prefixModeMap:Map<String, css.mode.Mode> = initModeMap();
	#end

	/*
	Assigns an implementation of Mode to handle a css namespace (vendor prefix)

	@arg mode The fully qualified class path
	@arg prefix The vendor prefix to handle
	 */
	public static function setModeForPrefix(mode:String, prefix:String)
	{
		importClass(mode);
		if(prefix == "default") modeClass = mode;
		var t = Type.resolveClass(mode);
		var inst = Type.createInstance(t, []);
		prefixModeMap.set(prefix, inst);
	}

	static var modeClass:String = "css.mode.DefaultMode";
	/*
	public static function setMode(value:String)
	{
		modeClass = value;
		importClass(value);
	}
    */
	public static var mode:css.mode.Mode;

	static function getMode():css.mode.Mode
	{
		if (mode == null)
		{
			var t = Type.resolveClass(modeClass);
			if (t == null)
			{
				haxe.macro.Context.error(
					modeClass + " cannot be resolved. You probably need to add a reference somewhere in the app",
					haxe.macro.Context.currentPos()
				);
			}
			mode = Type.createInstance(t, []);
		}
		return mode;
	}

	/*public static function eval(className:String)
	{
		if (resolver == null) throw "No resolver instance found. Did you specify e.g. '--macro css.CSS.styleSheet(['app.css'])' in your .hxml ?";

		var styleObject:RuleObject = resolver.getStyleObject(null, className);

		Sys.println("Properties for '" + className + "':");

		traceRules(styleObject);
	}*/

	#end

	/**
	 * Set (and trace) target properties matching the local type, target type and class names
	 */
	#if !haxe3 @:macro #else macro #end
	public static function eval(target:Expr, ?classNames:String):Expr
	{
		if (haxe.macro.Context.defined("display")) return { expr:EBlock([]), pos:Context.currentPos() };
		return generateStyle(target, classNames, true);
	}
	
	/**
	 * Set target properties matching the local type, target type and class names
	 */
	#if !haxe3 @:macro #else macro #end
	public static function style(target:Expr, ?classNames:String):Expr
	{
		if (haxe.macro.Context.defined("display")) return { expr:EBlock([]), pos:Context.currentPos() };
		return generateStyle(target, classNames, false);
	}
	
	#if macro
	static function generateStyle(target:Expr, classNames:String, ?dump:Bool):Expr
	{
		if (resolver == null) throw "No resolver instance found. Did you specify e.g. '--macro css.CSS.styleSheet(['app.css'])' in your .hxml ?";

		var pos = Context.currentPos();

		var targetId:String = exprToText(target.expr, pos);
		if (targetId == null) return null;


		if (classNames == null) classNames = targetId.split(".").join("-");


		var localClass = Context.getLocalClass();
		var localName:String = "*";
		if (localClass != null) localName = localClass.get().name;


		var type = Context.typeof(target);
		var typeName = TypeInfo.getTypeNameFor(type);

		if(typeName == null) throw "Could not resolve Type name. Note that Dynamic object are not yet supported.";

		if (typeName.charAt(0) == "#") { // unbox type
			type = Context.getType(typeName.substr(1));
			typeName = TypeInfo.getTypeNameFor(type);
		}

		var isStatic = typeName == targetId;

		//var propertiesMap = TypeInfo.getPropertiesMapFor(type, isStatic);
		var styleObject:RuleObject = resolver.getStyleObjectInContext(localName, typeName, classNames);


		var block:Array<Expr> = [];

		for (prefix in styleObject.prefixes)
		{
			var stateMap:StateMap = styleObject.getPrefix(prefix);

			if (!prefixModeMap.exists(prefix)) throw "No mode defined for " + prefix;
			var m = prefixModeMap.get(prefix);

			var e = m.processSelector(type, typeName, classNames, target, targetId, stateMap);
			if(e == null) continue;
			block = block.concat(e);

		}

		if (dump)
		{
			Sys.println("Set props of " + targetId + ":" + typeName + " in " + localName + " for class name(s) '" + classNames + "':");
			traceRules(styleObject);
		}

		if(block.length == 0) return macro {};
		var e = {
			expr : EBlock(block),
			pos: pos
		}
		return e;
	}
	
	static private function exprToText(expr:ExprDef, pos:Position) 
	{
		var text = "";
		switch(expr)
		{
 			case EConst(c):
				switch(c)
				{
					case CIdent(s): return s;
					default:
						Context.error("Style target is not an identifier.", pos);
				}
			case EField(tail, s):
				return exprToText(tail.expr, pos) + "." + s;
				
			default:
				Context.error("Style target is an unsupported expression.", pos);
		}
		return null;
	}
	
	static private function traceRules(styleObject:RuleObject) 
	{
		for(declaration in styleObject.iterator())
		{
			var d:RuleDeclaration = declaration;
			var posInfo = " (" + d.pos.filename + ":" + d.pos.lineNumber + ":" + d.pos.char + ")";
			Sys.println("  " + d.name + " = " +d.value + posInfo);
		}
	}
	#end
}
