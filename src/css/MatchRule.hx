package css;

import pixelami.css.CSSParserToken;
import pixelami.css.CSSParserRule;

class MatchRule extends pixelami.css.CSSParserRule.StyleRule
{
	static inline var DEFAULT_PREFIX:String = "default";

	public var isValid:Bool;
	public var mask(default, null):Array<String>;

	var ruleObject:RuleObject;

	public function new(style:pixelami.css.CSSParserRule.StyleRule)
	{
		super();
		selector = style.selector;
		ruleType = style.ruleType;
		fillType = style.fillType;
		important = style.important;
		pos = style.pos;
		value = style.value;

		isValid = validate();
	}

	public function matches(localName:String, typeName:String, selector:String)
	{
		if (!isValid) return false;
		//trace(" with " + toSelector());

		if (mask[0] != "*" && mask[0] != localName) return false;
		if (mask[1] != "*" && mask[1] != typeName) return false;
		if (mask[2] != selector) return false;
		return true;
	}

	public function toObject(file:String):RuleObject
	{
		if (ruleObject == null) ruleObject = createRuleObject(file);
		return ruleObject;
	}

	function createRuleObject(file:String):RuleObject
	{
		var ruleObject = new RuleObject();
		var stateName = getPseudoClassName();
		for (item in value)
		{
			if(Std.is(item, pixelami.css.Declaration))
			{
				var declaration:Declaration = cast item;
				declaration.pos.filename = file;
				var prefixAndName = splitPrefixAndName(declaration.name);
				var name = parseRuleItemName(prefixAndName.name);
				var value = getDeclarationValue(cast declaration.value);

				if (value == "") continue;

				if (prefixAndName.prefix == "")
				{
					ruleObject.add(DEFAULT_PREFIX, name, stateName, value, declaration.pos);
					//warn("ignoring item with no prefix " + item);
				}
				else
				{
					ruleObject.add(prefixAndName.prefix, stateName, name, value, declaration.pos);
				}
			}
			else
			{
				warn("Currently only supporting CSS Declarations - unsupported:" + Type.getClassName(Type.getClass(item)));
			}
		}
		return ruleObject;
	}

	function getPseudoClassName():String
	{
		var pos = 0;
		while(pos < selector.length)
		{
			if(matchesTypeAndValue(selector[pos], TokenType.DELIMITER, ":"))
			{
				var next:CSSParserToken = selector[pos+1];
				if(next.tokenType == TokenType.IDENTIFIER) return next.value;
			}
			pos++;
		}
		return "*";
	}

	function matchesTypeAndValue(token:CSSParserToken, tokenType:TokenType, value:Dynamic):Bool
	{
		return token.tokenType == tokenType && token.value == value;
	}

	function getDeclarationValue(tokens:Array<pixelami.css.CSSParserToken>)
	{
		var i = -1;
		var len = tokens.length;
		var value:String = "";
		var skipWS = true;

		while (i++ < len)
		{
			var token = tokens[i];
			if (token == null) continue;

			if (Std.is(token, SimpleBlock))
			{
				var block:SimpleBlock = cast token;
				warn("Sub-expressions in CSS attributes are not supported (parenthesis, square brackets)");
				return "";
			}

			if (skipWS) {
				if (token.tokenType != TokenType.WHITESPACE) skipWS = false;
				else continue;
			}
			if (i == len - 1 && token.tokenType == TokenType.WHITESPACE) break;

			var part = parseTokenValue(token);
			if (part != null) value += part;
		}
		trace("value: "+value);
		return value;
	}

	function hasPrefix(name:String):Bool
	{
		return StringTools.startsWith(name, "-");
	}

	function splitPrefixAndName(name:String):{prefix:String, name:String}
	{
		if (hasPrefix(name))
		{
			var idx = name.indexOf("-", 1);
			var prefix = name.substr(1, idx - 1);
			var n = name.substr(idx + 1);
			return {prefix:prefix, name:n};
		}
		return {prefix:DEFAULT_PREFIX, name:name};
	}

	function parseRuleItemName(name:String):String
	{
		// converts hyphenated names to camelCase
		var p:EReg = ~/-([a-z])/;

		#if !haxe3
		var n = p.customReplace(name, function(e:EReg):String
		{
			return e.matched(1).toUpperCase();
		});
		#else
		var n = p.map(name, function(e:EReg):String
		{
			return e.matched(1).toUpperCase();
		});
		#end


		return n;
	}

	function getFirstNonWhitespaceToken(tokens:Array<pixelami.css.CSSParserToken>):CSSParserToken
	{
		var i = -1;
		while (i++ < tokens.length)
		{
			if (tokens[i].tokenType != TokenType.WHITESPACE)
			{
				return tokens[i];
			}
		}
		return null;
	}

	function parseTokenValue(token:CSSParserToken):Dynamic
	{
		//warn("token: "+token);
		trace("token:"+token);
		var value:Dynamic = null;
		try
		{
			value = switch(token.tokenType)
			{
				case TokenType.HASH : "0x" + token.value;
				case TokenType.NUMBER : token.value;
				case TokenType.DIMENSION : untyped token.num;
				case TokenType.STRING : "\"" + token.value + "\"";
				case TokenType.IDENTIFIER : token.value;
				case TokenType.WHITESPACE: " ";
				case TokenType.DELIMITER: String.fromCharCode(token.value);
				default: null;
			}
		}
		catch(e:Dynamic)
		{
			warn(e);
		}
		return value;
	}

	function validate()
	{
		var tokens:Array<pixelami.css.CSSParserToken> = cast selector;
		//trace("VALID? " + tokens.length + " " + tokens);

		// matching selectors as "View Text.title"
		if (tokens.length == 0) mask = []
		else if (tokens.length < 3  && tokens[0].tokenType == TokenType.IDENTIFIER) // ie. "Text"
		{
			mask = ["*", tokens[0].value, "*"];
			//trace("FIXED1: " + mask);
		}
		else if (tokens[0].tokenType == TokenType.DELIMITER && tokens[0].value == 46
		&& tokens[1].tokenType == TokenType.IDENTIFIER) // ie. ".title"
		{
			mask = ["*", "*", tokens[1].value];
			//trace("FIXED2: " + mask);
		}
		else if (tokens.length >= 3)
		{
			if (tokens[1].tokenType == TokenType.DELIMITER && tokens[1].value == 46) // ie. "Text.title"
			{
				mask = ["*", tokens[0].value, tokens[2].value];
				//trace("FIXED3: " + mask);
			}
			else if (tokens[1].tokenType == TokenType.WHITESPACE)
			{
				if (tokens[2].tokenType == TokenType.DELIMITER && tokens[2].value == 46) // ie. "View .foo"
				{
					mask = [tokens[0].value, "*", tokens[3].value];
					//trace("FIXED4: " + mask);
				}
				else // ie. "View Text"
				{
					mask = [tokens[0].value, tokens[2].value, "*"];
					//trace("FIXED5: " + mask);
				}
			}
			else mask = [];
		}
		else mask = [];

		if (mask.length == 0) // invalid
		{
			warn("Rule dropped " + tokens.toString());
			return false;
		}
		else return true;
	}

	public function toSelector()
	{
		return mask[0] + " " + mask[1] + "." + mask[2];
	}

	function warn(message:String)
	{
		//trace(message);
		#if macro haxe.macro.Context.warning(message, haxe.macro.Context.currentPos()); #end
	}
}
