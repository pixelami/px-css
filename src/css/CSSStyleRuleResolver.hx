package css;

import css.RuleObject;
import css.RuleObject.RuleDeclaration;

import pixelami.css.Tokenizer;
import pixelami.css.Parser;

class CSSStyleRuleResolver
{
	var stylesheets:Array<StylesheetFile>;

	public function new(stylesheets:Array<StylesheetFile>)
	{
		this.stylesheets = stylesheets;
	}

	public function getStyleObject(typeName:String, styleName:String):RuleObject
	{
		return getStyleObjectInContext("*", typeName, styleName);
	}

	public function getStyleObjectInContext(localName:String, typeName:String, styleName:String):RuleObject
	{
		var style:RuleObject = new RuleObject();

		// First resolve any rule that might apply to the Type.
		for (ruleObject in ruleMatch(localName, typeName, "*"))
		{
			mergeRules(ruleObject, style);
		}

		// Now resolve the rules for the selectors.
		var selectors = parseStyleName(styleName);
		//warn("selectors: "+selectors);
		for (selector in selectors)
		{
			if (selector == null || selector == "") continue;

			var matches:Array<RuleObject> = ruleMatch(localName, typeName, selector);
			if (matches.length == 0)
			{
				warn(selector + " not found");
				continue;
			}
			for (ruleObject in matches)
			{
				mergeRules(ruleObject, style);
			}
		}
		return style;
	}

	function mergeRules(ruleObject:RuleObject, targetStyle:RuleObject)
	{
		//trace("rule: "+ruleObject);
		for(declaration in ruleObject.iterator())
		{
			var d:RuleDeclaration = declaration;
			targetStyle.add(d.prefix, d.state,  d.name, d.value, d.pos);
		}
	}

	function parseStyleName(styleName:String):Array<String>
	{
		if (styleName == null || styleName == "") return [];
		return styleName.split(" ");
	}

	function ruleMatch(localName:String, typeName:String, selector:String):Array<RuleObject>
	{
		//warn("Matching " + localName + " " + typeName + "." + selector);
		if (selector == "") return null;

		var matched:Array<RuleObject> = [];

		// iterate over the stylesheets
		var l = stylesheets.length;
		var i = -1;
		while (++i < l)
		{
			var stylesheetFile = stylesheets[i];

			var rules = stylesheetFile.stylesheet.value;
			var len = rules.length;
			for (j in 0...len)
			{
				var style = rules[j];
				var rule:MatchRule;
				if (!Std.is(style, MatchRule)) rules[j] = rule = new MatchRule(style);
				else rule = cast style;

				if (rule.matches(localName, typeName, selector))
					matched.push( rule.toObject(stylesheetFile.file) );

			}
		}

		return matched;
	}

	function warn(message:String)
	{
		//trace(message);
		#if macro haxe.macro.Context.warning(message, haxe.macro.Context.currentPos()); #end
	}

}

