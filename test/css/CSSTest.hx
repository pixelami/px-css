package css;

import css.mode.Mode;
import css.helper.TestComponent2;
import css.helper.TestComponent;
import massive.munit.Assert;

class CSSTest
{
	@Test
	public function rulesShouldBeAppliedToTestComponent()
	{
		var c = new TestComponent();
		css.CSS.style(c, "test");

		Assert.areEqual(0xFF0000, c.color);
		Assert.areEqual("left", c.orientation);
		Assert.areEqual(true, c.emphasis);
		Assert.areEqual(60, c.width);
	}

	@Test
	public function emptyCSSDeclarationShouldNotThrowError()
	{
		// TestComponent2 contains a TextComponent instance and tries to apply the style based on the localClass
		// i.e.
		// TextComponent2 .titleText
		new TestComponent2();

	}

	// 'need to find way to test at macro time'
	@Ignore
	@Test
	public function prefixStyleMapShouldContainDefaultMapping()
	{

		//var prefixModeMap:Map<String,Mode> = CSS.prefixModeMap;
		//trace("prefixModeMap: "+prefixModeMap);
		//Assert.isNotNull(prefixModeMap.get("default"));

	}
}
