package css;

import css.RuleObject;
import pixelami.css.CSSParserRule;
import massive.munit.Assert;
import pixelami.css.Tokenizer;
import pixelami.css.Parser;

class CSSStyleRuleResolverTest
{

	var resolver:CSSStyleRuleResolver;
	var stylesheets:Array<StylesheetFile>;

	@BeforeClass
	public function beforeClass():Void
	{

		stylesheets = [];
		var css1 = haxe.Resource.getString("css1");
		var t = new Tokenizer(css1);
		var p = new Parser(t.tokens);
		stylesheets.push({stylesheet:p.stylesheet, file:"css1"});

		var css2 = haxe.Resource.getString("css2");
		var t = new Tokenizer(css2);
		var p = new Parser(t.tokens);
		stylesheets.push({stylesheet:p.stylesheet, file:"css2"});

	}

	@Before
	public function setup():Void
	{
		resolver = new CSSStyleRuleResolver(stylesheets);
	}

	@After
	public function tearDown():Void
	{
	}


	//@Ignore
	@Test
	public function lastCSSFileShouldTakePrecedenceWhenResolvingSelectors():Void
	{
	    trace("test1");
		// we expect that since css2 defines '.app' then it should override any '.app' selector defined in css1
		var s:RuleObject = resolver.getStyleObject(null, "app");

		var ruleMap:RuleMap = s.getStateForPrefix("foo");
		//trace("rulemap: "+ruleMap);
		var declaration:RuleDeclaration = ruleMap.get("fill");
		var value:Dynamic = declaration.value;

		Assert.areEqual("0xFF0000", value);

	}


	//@Ignore
	@Test
	public function lastTypeSelectorDefinitionShouldTakePrecedence():Void
	{
		var s = resolver.getStyleObject("Text", "h1");
		var value:Dynamic = s.getStateForPrefix("foo").get("leading").value;
		Assert.areEqual("-6", value);
	}


	//@Ignore
	@Test
	public function typeSelectorDefinitionShouldBeOverriddenByClassSelector():Void
	{
		// in this case the 'leading' value defined in '.h2' overrides the 'leading' value defined in 'Text'
		var s = resolver.getStyleObject("Text", "h2");
		var value:Dynamic = s.getStateForPrefix("foo").get("leading").value;
		Assert.areEqual("2", value);
	}


	//@Ignore
	@Test
	public function styleObjectShouldContainPrefixes():Void
	{
		// in this case the 'leading' value defined in '.h2' overrides the 'leading' value defined in 'Text'
		var s = resolver.getStyleObject("Text", "h2");

		var l = 0;
		for (p in s.prefixes)
		{
			l++;
		}

		Assert.areEqual(1, l);

	}
}
