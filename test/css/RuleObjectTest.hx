package css;

import css.RuleObject;
import pixelami.css.CSSParserToken;
import massive.munit.Assert;

class RuleObjectTest
{
	var ruleObject:RuleObject;

	@Before
	public function before()
	{
		ruleObject = new RuleObject();
	}


	//@Ignore
	@Test
	public function iteratorShouldIterate()
	{
		ruleObject.add("default", "*", "foo", "bar", makePos());
		ruleObject.add("default", "boo", "foo", "bar", makePos());

		var iterator = ruleObject.iterator();
		var l = 0;
		trace("next: "+iterator.hasNext());
		while(iterator.hasNext())
		{
			iterator.next();
			l++;
		}
		Assert.areEqual(2, l);

	}


	//@Ignore
	@Test
	public function mergeTest()
	{
		ruleObject.add("default", "*", "foo", "bar", makePos());
		ruleObject.add("default", "boo", "foo", "bar", makePos());

		var ruleObject2 = new RuleObject();
		ruleObject2.add("default", "*", "foo", "bar2", makePos());
		ruleObject2.add("default", "boo", "foo", "bar2", makePos());

		var iterator = ruleObject2.iterator();
		while(iterator.hasNext())
		{
			var d:RuleDeclaration = iterator.next();
			ruleObject.add(d.prefix, d.state, d.name, d.value, d.pos);
		}

		var ruleMap:RuleMap = ruleObject.getStateForPrefix("default");
		Assert.areEqual("bar2", ruleMap.get("foo").value);

		var ruleMap:RuleMap = ruleObject.getStateForPrefix("default", "boo");
		Assert.areEqual("bar2", ruleMap.get("foo").value);
	}

	function makePos():Position
	{
		return {lineNumber:1, filename:"", char:0};
	}
}
