import massive.munit.TestSuite;

import css.CSSStyleRuleResolverTest;
import css.CSSTest;
import css.mode.ModeTest;
import css.RuleObjectTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */

class TestSuite extends massive.munit.TestSuite
{		

	public function new()
	{
		super();

		add(css.CSSStyleRuleResolverTest);
		add(css.CSSTest);
		add(css.mode.ModeTest);
		add(css.RuleObjectTest);
	}
}
